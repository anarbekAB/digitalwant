$(document).ready(function () {
    //send comment form ajax
    $(document).on("click", "#send-comment", function (e) {
        var form = $("#form-comment");
        e.preventDefault();

        data = form.serialize();

        $(this).hide();
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                $(this).show();

                if (data.resultComment.status === 'Y') {
                    $('.comment-container').append(
                        '<div class="callout primary">' +
                            '<div class="media-object-section">' +
                                '<a name="comment_'+ data.resultComment.id +'"></a>'+
                                '<h5>'+ data.resultComment.username +
                                    '<small> '+ data.resultComment.date +' </small>'+
                                '</h5>'+
                                '<div class="col-sm-9">' + data.resultComment.text + '</div>' +
                        '</div>'+
                        '</div>' );
                    var oldCount = parseInt($('#comment-count').html());
                    $('#comment-count').html(oldCount + 1);
                }
                if (data.resultComment.status === 'N') {

                }
            }
        });
        $(this).show();

    });

    //show all comments and form
    $(document).on("click", "#comment-view", function (e) {
        updateComment();
    });
});

function updateComment() {
    var url = $('#comment-view').data('url');
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            $('.news-detail').append(data);
        }
    });
}