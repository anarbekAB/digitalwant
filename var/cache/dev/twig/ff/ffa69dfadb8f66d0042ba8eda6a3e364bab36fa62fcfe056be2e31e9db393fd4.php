<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_5bb381d2016f4fb31ed76213b789caa25a3f81b0f29e559643b0dd7ce9f134c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a8df9df8cc7b65f53ec6ddb3a774d7ec65b472f66395d63e6297d1b7f85a160 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a8df9df8cc7b65f53ec6ddb3a774d7ec65b472f66395d63e6297d1b7f85a160->enter($__internal_1a8df9df8cc7b65f53ec6ddb3a774d7ec65b472f66395d63e6297d1b7f85a160_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_cf26edc54cbb68f5b70a10be141798172b0152f1b86dc10f1e1dfdb7bd1ec6b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf26edc54cbb68f5b70a10be141798172b0152f1b86dc10f1e1dfdb7bd1ec6b2->enter($__internal_cf26edc54cbb68f5b70a10be141798172b0152f1b86dc10f1e1dfdb7bd1ec6b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_1a8df9df8cc7b65f53ec6ddb3a774d7ec65b472f66395d63e6297d1b7f85a160->leave($__internal_1a8df9df8cc7b65f53ec6ddb3a774d7ec65b472f66395d63e6297d1b7f85a160_prof);

        
        $__internal_cf26edc54cbb68f5b70a10be141798172b0152f1b86dc10f1e1dfdb7bd1ec6b2->leave($__internal_cf26edc54cbb68f5b70a10be141798172b0152f1b86dc10f1e1dfdb7bd1ec6b2_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_91dc0f13188ce16c3641a9ddc7ede5c3bc7db00c843a93f73723bd35c21637ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91dc0f13188ce16c3641a9ddc7ede5c3bc7db00c843a93f73723bd35c21637ed->enter($__internal_91dc0f13188ce16c3641a9ddc7ede5c3bc7db00c843a93f73723bd35c21637ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b790d5b05cff81a44774e08fb26003acad9de64abd9dae3361f342e11c8d9f34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b790d5b05cff81a44774e08fb26003acad9de64abd9dae3361f342e11c8d9f34->enter($__internal_b790d5b05cff81a44774e08fb26003acad9de64abd9dae3361f342e11c8d9f34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_b790d5b05cff81a44774e08fb26003acad9de64abd9dae3361f342e11c8d9f34->leave($__internal_b790d5b05cff81a44774e08fb26003acad9de64abd9dae3361f342e11c8d9f34_prof);

        
        $__internal_91dc0f13188ce16c3641a9ddc7ede5c3bc7db00c843a93f73723bd35c21637ed->leave($__internal_91dc0f13188ce16c3641a9ddc7ede5c3bc7db00c843a93f73723bd35c21637ed_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_98e2bca6559867b12efc09b3899262a857897a8882f78900867a2ba0b2c1c2f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_98e2bca6559867b12efc09b3899262a857897a8882f78900867a2ba0b2c1c2f4->enter($__internal_98e2bca6559867b12efc09b3899262a857897a8882f78900867a2ba0b2c1c2f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_571c8369fc8da98ff51637f452916b887a226db66c79b1eae709e78a189566de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_571c8369fc8da98ff51637f452916b887a226db66c79b1eae709e78a189566de->enter($__internal_571c8369fc8da98ff51637f452916b887a226db66c79b1eae709e78a189566de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_571c8369fc8da98ff51637f452916b887a226db66c79b1eae709e78a189566de->leave($__internal_571c8369fc8da98ff51637f452916b887a226db66c79b1eae709e78a189566de_prof);

        
        $__internal_98e2bca6559867b12efc09b3899262a857897a8882f78900867a2ba0b2c1c2f4->leave($__internal_98e2bca6559867b12efc09b3899262a857897a8882f78900867a2ba0b2c1c2f4_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_18b7e6f4199ece3e278041925448e3d0aad35f7d6976dc5f5805b3018cbd37bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18b7e6f4199ece3e278041925448e3d0aad35f7d6976dc5f5805b3018cbd37bc->enter($__internal_18b7e6f4199ece3e278041925448e3d0aad35f7d6976dc5f5805b3018cbd37bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1327c4959bd066bd2610823e298e560c2dc900ab98edbe9828eb9b7582f01302 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1327c4959bd066bd2610823e298e560c2dc900ab98edbe9828eb9b7582f01302->enter($__internal_1327c4959bd066bd2610823e298e560c2dc900ab98edbe9828eb9b7582f01302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1327c4959bd066bd2610823e298e560c2dc900ab98edbe9828eb9b7582f01302->leave($__internal_1327c4959bd066bd2610823e298e560c2dc900ab98edbe9828eb9b7582f01302_prof);

        
        $__internal_18b7e6f4199ece3e278041925448e3d0aad35f7d6976dc5f5805b3018cbd37bc->leave($__internal_18b7e6f4199ece3e278041925448e3d0aad35f7d6976dc5f5805b3018cbd37bc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/anarvek/PhpstormProjects/digitalwand/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
