<?php

/* base.html.twig */
class __TwigTemplate_ef6a598e82a5011af52d5b328f0cd2f8830bea5f4c88506ad9e23671aec9697a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d5f525deb153ad8d0eeeea305a75d186460b067c8d3e44c0fc6d8af6ce3ba77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d5f525deb153ad8d0eeeea305a75d186460b067c8d3e44c0fc6d8af6ce3ba77->enter($__internal_9d5f525deb153ad8d0eeeea305a75d186460b067c8d3e44c0fc6d8af6ce3ba77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_185ae86abf84e8f71090e6405e9fe73dec0effb2c50e875db945372babd122a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_185ae86abf84e8f71090e6405e9fe73dec0effb2c50e875db945372babd122a7->enter($__internal_185ae86abf84e8f71090e6405e9fe73dec0effb2c50e875db945372babd122a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_9d5f525deb153ad8d0eeeea305a75d186460b067c8d3e44c0fc6d8af6ce3ba77->leave($__internal_9d5f525deb153ad8d0eeeea305a75d186460b067c8d3e44c0fc6d8af6ce3ba77_prof);

        
        $__internal_185ae86abf84e8f71090e6405e9fe73dec0effb2c50e875db945372babd122a7->leave($__internal_185ae86abf84e8f71090e6405e9fe73dec0effb2c50e875db945372babd122a7_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_781462d03dcdd623466c49d408d5a7a373abb2dccdb1bb53acbb6768f7e6effc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_781462d03dcdd623466c49d408d5a7a373abb2dccdb1bb53acbb6768f7e6effc->enter($__internal_781462d03dcdd623466c49d408d5a7a373abb2dccdb1bb53acbb6768f7e6effc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_957a51085be4196af54ac299948e18f2fdb8bb9a853295c84733e5c02c5d710f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_957a51085be4196af54ac299948e18f2fdb8bb9a853295c84733e5c02c5d710f->enter($__internal_957a51085be4196af54ac299948e18f2fdb8bb9a853295c84733e5c02c5d710f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_957a51085be4196af54ac299948e18f2fdb8bb9a853295c84733e5c02c5d710f->leave($__internal_957a51085be4196af54ac299948e18f2fdb8bb9a853295c84733e5c02c5d710f_prof);

        
        $__internal_781462d03dcdd623466c49d408d5a7a373abb2dccdb1bb53acbb6768f7e6effc->leave($__internal_781462d03dcdd623466c49d408d5a7a373abb2dccdb1bb53acbb6768f7e6effc_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_72ba1a5527dda33846de86fdc8a72924cd5d91b7e91f1157fb55da2584ba0eec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72ba1a5527dda33846de86fdc8a72924cd5d91b7e91f1157fb55da2584ba0eec->enter($__internal_72ba1a5527dda33846de86fdc8a72924cd5d91b7e91f1157fb55da2584ba0eec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3fb067d34b76414bb6bc73f366bb27b1b3930de8a217da1bf07385f14ca62f8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fb067d34b76414bb6bc73f366bb27b1b3930de8a217da1bf07385f14ca62f8a->enter($__internal_3fb067d34b76414bb6bc73f366bb27b1b3930de8a217da1bf07385f14ca62f8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3fb067d34b76414bb6bc73f366bb27b1b3930de8a217da1bf07385f14ca62f8a->leave($__internal_3fb067d34b76414bb6bc73f366bb27b1b3930de8a217da1bf07385f14ca62f8a_prof);

        
        $__internal_72ba1a5527dda33846de86fdc8a72924cd5d91b7e91f1157fb55da2584ba0eec->leave($__internal_72ba1a5527dda33846de86fdc8a72924cd5d91b7e91f1157fb55da2584ba0eec_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_131b625387fef684da767d89c54514f6cef9138ca39993290ab2516bddfa2fec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_131b625387fef684da767d89c54514f6cef9138ca39993290ab2516bddfa2fec->enter($__internal_131b625387fef684da767d89c54514f6cef9138ca39993290ab2516bddfa2fec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9b9642ff32fa8be0a4ab59c0a0813fe193c43a6557756286adebac6c1beebce2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b9642ff32fa8be0a4ab59c0a0813fe193c43a6557756286adebac6c1beebce2->enter($__internal_9b9642ff32fa8be0a4ab59c0a0813fe193c43a6557756286adebac6c1beebce2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_9b9642ff32fa8be0a4ab59c0a0813fe193c43a6557756286adebac6c1beebce2->leave($__internal_9b9642ff32fa8be0a4ab59c0a0813fe193c43a6557756286adebac6c1beebce2_prof);

        
        $__internal_131b625387fef684da767d89c54514f6cef9138ca39993290ab2516bddfa2fec->leave($__internal_131b625387fef684da767d89c54514f6cef9138ca39993290ab2516bddfa2fec_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e45748675ddfa6ceec0132f1cd7ae8f12b142e08c5deb8e136dbc094ec64acc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e45748675ddfa6ceec0132f1cd7ae8f12b142e08c5deb8e136dbc094ec64acc0->enter($__internal_e45748675ddfa6ceec0132f1cd7ae8f12b142e08c5deb8e136dbc094ec64acc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_cd4eba21b6d33bf3724bab9c0c6022f3bc20466ca1c50eadc37a98dcd5719e8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd4eba21b6d33bf3724bab9c0c6022f3bc20466ca1c50eadc37a98dcd5719e8d->enter($__internal_cd4eba21b6d33bf3724bab9c0c6022f3bc20466ca1c50eadc37a98dcd5719e8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_cd4eba21b6d33bf3724bab9c0c6022f3bc20466ca1c50eadc37a98dcd5719e8d->leave($__internal_cd4eba21b6d33bf3724bab9c0c6022f3bc20466ca1c50eadc37a98dcd5719e8d_prof);

        
        $__internal_e45748675ddfa6ceec0132f1cd7ae8f12b142e08c5deb8e136dbc094ec64acc0->leave($__internal_e45748675ddfa6ceec0132f1cd7ae8f12b142e08c5deb8e136dbc094ec64acc0_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/anarvek/PhpstormProjects/digitalwand/app/Resources/views/base.html.twig");
    }
}
