<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_2818adb03de0349061047d8de12e1cb1f6b314a5fb9b9431c7361d075fded912 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cbceda2b501034a437b5b8273c1af1743f3715015c1e5f7f641270c4819fc4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cbceda2b501034a437b5b8273c1af1743f3715015c1e5f7f641270c4819fc4f->enter($__internal_3cbceda2b501034a437b5b8273c1af1743f3715015c1e5f7f641270c4819fc4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_2d5e0323edf6ee9332fe116b0467f0ab4ad2118295c4708db1514c2254ae0170 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d5e0323edf6ee9332fe116b0467f0ab4ad2118295c4708db1514c2254ae0170->enter($__internal_2d5e0323edf6ee9332fe116b0467f0ab4ad2118295c4708db1514c2254ae0170_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_3cbceda2b501034a437b5b8273c1af1743f3715015c1e5f7f641270c4819fc4f->leave($__internal_3cbceda2b501034a437b5b8273c1af1743f3715015c1e5f7f641270c4819fc4f_prof);

        
        $__internal_2d5e0323edf6ee9332fe116b0467f0ab4ad2118295c4708db1514c2254ae0170->leave($__internal_2d5e0323edf6ee9332fe116b0467f0ab4ad2118295c4708db1514c2254ae0170_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/home/anarvek/PhpstormProjects/digitalwand/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
