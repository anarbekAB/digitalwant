<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_5ace0514ac51dddf7015e278581d1ccbc34e9ce9c04fa0c5adac0bc55a3fad5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_715242e6ee40c15cf7fa3ef10b278cbb8e8bf276b92fe57cf3f7a8d0948607c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_715242e6ee40c15cf7fa3ef10b278cbb8e8bf276b92fe57cf3f7a8d0948607c7->enter($__internal_715242e6ee40c15cf7fa3ef10b278cbb8e8bf276b92fe57cf3f7a8d0948607c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_526a0d9ce0be3d7eef4b62f6d9cd62d6e5785c46c2fe417a5dca5ac40a09b012 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_526a0d9ce0be3d7eef4b62f6d9cd62d6e5785c46c2fe417a5dca5ac40a09b012->enter($__internal_526a0d9ce0be3d7eef4b62f6d9cd62d6e5785c46c2fe417a5dca5ac40a09b012_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_715242e6ee40c15cf7fa3ef10b278cbb8e8bf276b92fe57cf3f7a8d0948607c7->leave($__internal_715242e6ee40c15cf7fa3ef10b278cbb8e8bf276b92fe57cf3f7a8d0948607c7_prof);

        
        $__internal_526a0d9ce0be3d7eef4b62f6d9cd62d6e5785c46c2fe417a5dca5ac40a09b012->leave($__internal_526a0d9ce0be3d7eef4b62f6d9cd62d6e5785c46c2fe417a5dca5ac40a09b012_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7e16447ddab6a65ae770a5f09e84b2e66f2d737f94fee368369faf404f478f9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e16447ddab6a65ae770a5f09e84b2e66f2d737f94fee368369faf404f478f9a->enter($__internal_7e16447ddab6a65ae770a5f09e84b2e66f2d737f94fee368369faf404f478f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_9e55ec71de6f8405c81ca2b17469fbd09fa16cd091e9918d59ed272cf7ddcf7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e55ec71de6f8405c81ca2b17469fbd09fa16cd091e9918d59ed272cf7ddcf7f->enter($__internal_9e55ec71de6f8405c81ca2b17469fbd09fa16cd091e9918d59ed272cf7ddcf7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_9e55ec71de6f8405c81ca2b17469fbd09fa16cd091e9918d59ed272cf7ddcf7f->leave($__internal_9e55ec71de6f8405c81ca2b17469fbd09fa16cd091e9918d59ed272cf7ddcf7f_prof);

        
        $__internal_7e16447ddab6a65ae770a5f09e84b2e66f2d737f94fee368369faf404f478f9a->leave($__internal_7e16447ddab6a65ae770a5f09e84b2e66f2d737f94fee368369faf404f478f9a_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_38123ca3b52d317900e8bf5d21b0e6d91dee1b2d6b9c4eb4f8a67486ac1bfd05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38123ca3b52d317900e8bf5d21b0e6d91dee1b2d6b9c4eb4f8a67486ac1bfd05->enter($__internal_38123ca3b52d317900e8bf5d21b0e6d91dee1b2d6b9c4eb4f8a67486ac1bfd05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_43d37c282ecd32c296fbd8090545acf6b915913e39ca17f5cdd05efc8fc9eda2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43d37c282ecd32c296fbd8090545acf6b915913e39ca17f5cdd05efc8fc9eda2->enter($__internal_43d37c282ecd32c296fbd8090545acf6b915913e39ca17f5cdd05efc8fc9eda2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_43d37c282ecd32c296fbd8090545acf6b915913e39ca17f5cdd05efc8fc9eda2->leave($__internal_43d37c282ecd32c296fbd8090545acf6b915913e39ca17f5cdd05efc8fc9eda2_prof);

        
        $__internal_38123ca3b52d317900e8bf5d21b0e6d91dee1b2d6b9c4eb4f8a67486ac1bfd05->leave($__internal_38123ca3b52d317900e8bf5d21b0e6d91dee1b2d6b9c4eb4f8a67486ac1bfd05_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_8b111adf4a358e1d2b03fc3aad94eda63ee15376687bdd3871e80769cf13d915 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b111adf4a358e1d2b03fc3aad94eda63ee15376687bdd3871e80769cf13d915->enter($__internal_8b111adf4a358e1d2b03fc3aad94eda63ee15376687bdd3871e80769cf13d915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6a2ab062f49bd747763b4cc9e7fd41c506cf1573653e47e4c33fcc0e35c5d197 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a2ab062f49bd747763b4cc9e7fd41c506cf1573653e47e4c33fcc0e35c5d197->enter($__internal_6a2ab062f49bd747763b4cc9e7fd41c506cf1573653e47e4c33fcc0e35c5d197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_6a2ab062f49bd747763b4cc9e7fd41c506cf1573653e47e4c33fcc0e35c5d197->leave($__internal_6a2ab062f49bd747763b4cc9e7fd41c506cf1573653e47e4c33fcc0e35c5d197_prof);

        
        $__internal_8b111adf4a358e1d2b03fc3aad94eda63ee15376687bdd3871e80769cf13d915->leave($__internal_8b111adf4a358e1d2b03fc3aad94eda63ee15376687bdd3871e80769cf13d915_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/anarvek/PhpstormProjects/digitalwand/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
