<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6c775e4c4ccdb3af11f3aa2031a382d5c0ed87f26f326c1f292fdbc62fcd4060 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0f127108629f6aca83fdea20a9dfa7dd0d0fac4a1c5468f83bf3e358a66f8ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0f127108629f6aca83fdea20a9dfa7dd0d0fac4a1c5468f83bf3e358a66f8ce->enter($__internal_b0f127108629f6aca83fdea20a9dfa7dd0d0fac4a1c5468f83bf3e358a66f8ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_03c6e933aedeb77de0b3f3549425c3b3953db7d677ab344d4bc89087282e7578 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03c6e933aedeb77de0b3f3549425c3b3953db7d677ab344d4bc89087282e7578->enter($__internal_03c6e933aedeb77de0b3f3549425c3b3953db7d677ab344d4bc89087282e7578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b0f127108629f6aca83fdea20a9dfa7dd0d0fac4a1c5468f83bf3e358a66f8ce->leave($__internal_b0f127108629f6aca83fdea20a9dfa7dd0d0fac4a1c5468f83bf3e358a66f8ce_prof);

        
        $__internal_03c6e933aedeb77de0b3f3549425c3b3953db7d677ab344d4bc89087282e7578->leave($__internal_03c6e933aedeb77de0b3f3549425c3b3953db7d677ab344d4bc89087282e7578_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4b9fd0304e684fc48b111d948985ab188133f4e87e52b9490c67f0de689b3778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b9fd0304e684fc48b111d948985ab188133f4e87e52b9490c67f0de689b3778->enter($__internal_4b9fd0304e684fc48b111d948985ab188133f4e87e52b9490c67f0de689b3778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_d3e4d65a812e2d1337bb903cc5fe4957b7b5cbecc9538b8687e4f0fca729f31d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3e4d65a812e2d1337bb903cc5fe4957b7b5cbecc9538b8687e4f0fca729f31d->enter($__internal_d3e4d65a812e2d1337bb903cc5fe4957b7b5cbecc9538b8687e4f0fca729f31d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_d3e4d65a812e2d1337bb903cc5fe4957b7b5cbecc9538b8687e4f0fca729f31d->leave($__internal_d3e4d65a812e2d1337bb903cc5fe4957b7b5cbecc9538b8687e4f0fca729f31d_prof);

        
        $__internal_4b9fd0304e684fc48b111d948985ab188133f4e87e52b9490c67f0de689b3778->leave($__internal_4b9fd0304e684fc48b111d948985ab188133f4e87e52b9490c67f0de689b3778_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_3481801f06ad39cf1d2ef1a855c91ec1041bb3867e9acd200b8cf6bcebb2bedf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3481801f06ad39cf1d2ef1a855c91ec1041bb3867e9acd200b8cf6bcebb2bedf->enter($__internal_3481801f06ad39cf1d2ef1a855c91ec1041bb3867e9acd200b8cf6bcebb2bedf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ad4da1f0e6b9d1a7f54234d183c915f2c62a129fb6f7452e2a1ca8b98842af59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad4da1f0e6b9d1a7f54234d183c915f2c62a129fb6f7452e2a1ca8b98842af59->enter($__internal_ad4da1f0e6b9d1a7f54234d183c915f2c62a129fb6f7452e2a1ca8b98842af59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ad4da1f0e6b9d1a7f54234d183c915f2c62a129fb6f7452e2a1ca8b98842af59->leave($__internal_ad4da1f0e6b9d1a7f54234d183c915f2c62a129fb6f7452e2a1ca8b98842af59_prof);

        
        $__internal_3481801f06ad39cf1d2ef1a855c91ec1041bb3867e9acd200b8cf6bcebb2bedf->leave($__internal_3481801f06ad39cf1d2ef1a855c91ec1041bb3867e9acd200b8cf6bcebb2bedf_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_bea50b10b4633d54fb60be8a46f266dd02dda1526bd25f33d5b9592fdc70ddfe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bea50b10b4633d54fb60be8a46f266dd02dda1526bd25f33d5b9592fdc70ddfe->enter($__internal_bea50b10b4633d54fb60be8a46f266dd02dda1526bd25f33d5b9592fdc70ddfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6d50f14201f394461c804273f9bd46b4499baefe1290c8fe51f4beb40add8716 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d50f14201f394461c804273f9bd46b4499baefe1290c8fe51f4beb40add8716->enter($__internal_6d50f14201f394461c804273f9bd46b4499baefe1290c8fe51f4beb40add8716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_6d50f14201f394461c804273f9bd46b4499baefe1290c8fe51f4beb40add8716->leave($__internal_6d50f14201f394461c804273f9bd46b4499baefe1290c8fe51f4beb40add8716_prof);

        
        $__internal_bea50b10b4633d54fb60be8a46f266dd02dda1526bd25f33d5b9592fdc70ddfe->leave($__internal_bea50b10b4633d54fb60be8a46f266dd02dda1526bd25f33d5b9592fdc70ddfe_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/anarvek/PhpstormProjects/digitalwand/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
