<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_4e3271903e33ece75271e854a17246dc43a169ac90a69a863843468c5ada6dd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd7346037db8474dd4d3a9f4de8886ba3340d20e7eaf040ec561152c9fa75049 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd7346037db8474dd4d3a9f4de8886ba3340d20e7eaf040ec561152c9fa75049->enter($__internal_bd7346037db8474dd4d3a9f4de8886ba3340d20e7eaf040ec561152c9fa75049_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_6a4c10c168720da67b067651ef57cd329fcb3a45ecf111397d46c961e1426ea1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a4c10c168720da67b067651ef57cd329fcb3a45ecf111397d46c961e1426ea1->enter($__internal_6a4c10c168720da67b067651ef57cd329fcb3a45ecf111397d46c961e1426ea1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd7346037db8474dd4d3a9f4de8886ba3340d20e7eaf040ec561152c9fa75049->leave($__internal_bd7346037db8474dd4d3a9f4de8886ba3340d20e7eaf040ec561152c9fa75049_prof);

        
        $__internal_6a4c10c168720da67b067651ef57cd329fcb3a45ecf111397d46c961e1426ea1->leave($__internal_6a4c10c168720da67b067651ef57cd329fcb3a45ecf111397d46c961e1426ea1_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_300f2e89956cb0fb1e2ea410375af019b1f3706ced147efbb6bc60174103530c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_300f2e89956cb0fb1e2ea410375af019b1f3706ced147efbb6bc60174103530c->enter($__internal_300f2e89956cb0fb1e2ea410375af019b1f3706ced147efbb6bc60174103530c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_0090170a1a603962eb96508a95e35eb4d4dffafbf7185db0e94143cac3ac9514 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0090170a1a603962eb96508a95e35eb4d4dffafbf7185db0e94143cac3ac9514->enter($__internal_0090170a1a603962eb96508a95e35eb4d4dffafbf7185db0e94143cac3ac9514_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_0090170a1a603962eb96508a95e35eb4d4dffafbf7185db0e94143cac3ac9514->leave($__internal_0090170a1a603962eb96508a95e35eb4d4dffafbf7185db0e94143cac3ac9514_prof);

        
        $__internal_300f2e89956cb0fb1e2ea410375af019b1f3706ced147efbb6bc60174103530c->leave($__internal_300f2e89956cb0fb1e2ea410375af019b1f3706ced147efbb6bc60174103530c_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a8a908ae1829a3a8efeb83f91752d1794d88dcb2ee75db69091e18a55d043700 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8a908ae1829a3a8efeb83f91752d1794d88dcb2ee75db69091e18a55d043700->enter($__internal_a8a908ae1829a3a8efeb83f91752d1794d88dcb2ee75db69091e18a55d043700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_0f7eb29ee8339f67d14524753ec4d593d8d15b8800b532f661ac017673a255d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f7eb29ee8339f67d14524753ec4d593d8d15b8800b532f661ac017673a255d2->enter($__internal_0f7eb29ee8339f67d14524753ec4d593d8d15b8800b532f661ac017673a255d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_0f7eb29ee8339f67d14524753ec4d593d8d15b8800b532f661ac017673a255d2->leave($__internal_0f7eb29ee8339f67d14524753ec4d593d8d15b8800b532f661ac017673a255d2_prof);

        
        $__internal_a8a908ae1829a3a8efeb83f91752d1794d88dcb2ee75db69091e18a55d043700->leave($__internal_a8a908ae1829a3a8efeb83f91752d1794d88dcb2ee75db69091e18a55d043700_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_3a3642da6685596e30eb1656019eb79f2e59730bdbce07f8aac4e5f43ec8128e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a3642da6685596e30eb1656019eb79f2e59730bdbce07f8aac4e5f43ec8128e->enter($__internal_3a3642da6685596e30eb1656019eb79f2e59730bdbce07f8aac4e5f43ec8128e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_aa93ba22ac7da652fd22aba86239c900589d2fd154adca8d759bf9015624e919 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa93ba22ac7da652fd22aba86239c900589d2fd154adca8d759bf9015624e919->enter($__internal_aa93ba22ac7da652fd22aba86239c900589d2fd154adca8d759bf9015624e919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_aa93ba22ac7da652fd22aba86239c900589d2fd154adca8d759bf9015624e919->leave($__internal_aa93ba22ac7da652fd22aba86239c900589d2fd154adca8d759bf9015624e919_prof);

        
        $__internal_3a3642da6685596e30eb1656019eb79f2e59730bdbce07f8aac4e5f43ec8128e->leave($__internal_3a3642da6685596e30eb1656019eb79f2e59730bdbce07f8aac4e5f43ec8128e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/anarvek/PhpstormProjects/digitalwand/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
