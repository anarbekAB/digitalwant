<?php
/**
 * Created by PhpStorm.
 * User: anarvek
 * Date: 06.12.17
 * Time: 20:49
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comments;
use AppBundle\Entity\News;
use AppBundle\Events;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class NewsController extends Controller{
	/**
	 * @Route("news/", defaults={"page": "1", "_format"="html"}, name="news_index")
	 * @Route("/page/{page}", defaults={"_format"="html"}, requirements={"page": "[1-9]\d*"}, name="news_index_paginated")
	 * @Method("GET")
	 * @Cache(smaxage="10")
	 */
	public function indexAction($page) {
		$em = $this->getDoctrine()->getManager();
		$news = $em->getRepository(News::class)->findLatest($page);

		return $this->render('news/index.html.twig', ['newsItems' => $news]);
	}

	/**
	 * @Route("/news/{slug}", name="news_post")
	 * @Method("GET")
	 */
	public function newsShowAction(News $news)
	{
//		if ('dev' === $this->getParameter('kernel.environment')) {
//			dump($news, $this->getUser(), new \DateTime());
//		}

		return $this->render('news/news_show.html.twig', ['newsItem' => $news]);
	}

	/**
	 * @Route("/comment/{postSlug}/new", name="comment_new")
	 * @Method({"GET", "POST"})
	 * @ParamConverter("news", options={"mapping": {"postSlug": "slug"}})
	 *
	 */
	public function commentNewAction(Request $request, News $news, EventDispatcherInterface $eventDispatcher)
	{
		$comment = new Comments();
		$news->addComment($comment);

		$form = $this->createForm(CommentType::class, $comment);
		$form->handleRequest($request);
		$arDataRes = [];


		if ($form->isSubmitted() && $form->isValid()) {
			$comment->setDateCreate(new \DateTime("now"));

			$em = $this->getDoctrine()->getManager();
			$em->persist($comment);
			$em->flush();

			$event = new GenericEvent($comment);

			$eventDispatcher->dispatch(Events::COMMENT_CREATED, $event);

			$arDataRes['id'] = $comment->getId();
			$arDataRes['username'] = $comment->getUsername();
			$arDataRes['text'] = $comment->getText();
			$arDataRes['date'] = $comment->getDateCreate()->format('d/m/Y');


			$arDataRes['status'] = 'Y';
//			return $this->redirectToRoute('news_post', ['slug' => $news->getSlug()]);
		} else {
			$arDataRes['status'] = 'N';
		}

		return new JsonResponse(['resultComment' => $arDataRes]);
	}

	/**
	 * @Route("/news/{postSlug}/show", name="comment_show")
	 * @ParamConverter("news", options={"mapping": {"postSlug": "slug"}})
	 *
	 * @Method({"GET", "POST"})
	 *
	 * @param News $news
	 *
	 * @return Response
	 */
	public function commentFormAction(News $news)
	{
		$form = $this->createForm(CommentType::class);

		return $this->render('news/_comment_form.html.twig', [
			'news' => $news,
			'form' => $form->createView(),
		]);
	}
}