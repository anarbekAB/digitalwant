<?php
/**
 * Created by PhpStorm.
 * User: anarvek
 * Date: 07.12.17
 * Time: 8:01
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Comments;
use AppBundle\Entity\News;
use AppBundle\Form\NewsType;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Utils\Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 */

class NewsController extends Controller{
	/**
	 * @Route("/", name="admin_index")
	 * @Route("/", name="admin_news_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$news = $em->getRepository(News::class)->findAll();

		return $this->render('admin/news/index.html.twig', ['newsItems' => $news]);
	}

	/**
	 * Creates a new News entity.
	 *
	 * @Route("/add", name="admin_news_new")
	 * @Method({"GET", "POST"})
	 *
	 */
	public function newAction(Request $request, Slugger $slugger)
	{
		$news = new News();

		$form = $this->createForm(NewsType::class, $news)
			->add('saveAndCreateNew', SubmitType::class);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$news->setSlug($slugger->slugify($news->getName()));
			$news->setDataCreate(new \DateTime("now"));


			$em = $this->getDoctrine()->getManager();
			$em->persist($news);
			$em->flush();


			$this->addFlash('success', 'Новость добавленна');

			if ($form->get('saveAndCreateNew')->isClicked()) {
				return $this->redirectToRoute('admin_news_new');
			}

			return $this->redirectToRoute('admin_news_index');
		}

		return $this->render('admin/news/add.html.twig', [
			'news' => $news,
			'form' => $form->createView(),
		]);
	}

	/**
	 * Deletes a News entity.
	 *
	 * @Route("/{id}/delete", name="admin_news_delete")
	 * @Method("POST")
	 *
	 * The Security annotation value is an expression (if it evaluates to false,
	 * the authorization mechanism will prevent the user accessing this resource).
	 */
	public function deleteAction(Request $request, News $news)
	{
		if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
			return $this->redirectToRoute('admin_news_index');
		}

		$em = $this->getDoctrine()->getManager();
		$em->remove($news);
		$em->flush();

		$this->addFlash('success', 'Запись удалена');

		return $this->redirectToRoute('admin_news_index');
	}



	/**
	 * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_news_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction(Request $request, News $news, Slugger $slugger)
	{
		$form = $this->createForm(NewsType::class, $news);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$news->setSlug($slugger->slugify($news->getName()));
			$this->getDoctrine()->getManager()->flush();

			$this->addFlash('success', 'Новость была отредактирована');

			return $this->redirectToRoute('admin_news_edit', ['id' => $news->getId()]);
		}

		return $this->render('admin/news/edit.html.twig', [
			'news' => $news,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/{id}/list", requirements={"id": "\d+"}, name="admin_comment_list")
	 * @Method({"GET", "POST"})
	 */
	public function listCommentAction(News $news)
	{

		return $this->render('admin/news/list_comment.html.twig', ['news' => $news]);
	}




	/**
	 * @Route("/{id}/list/comment/{commentId}/edit", name="admin_comment_edit")
	 * @Method({"GET", "POST"})
	 * @ParamConverter("news", class="AppBundle:News", options={"mapping": {"id" : "id"}})
	 * @ParamConverter("comment", class="AppBundle:Comments", options={"mapping": {"commentId" : "id"}})
	 *
	 */
	public function commentEditAction(Request $request, Comments $comment, News $news)
	{
		$form = $this->createForm(CommentType::class, $comment);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$comment->setDateCreate(new \DateTime("now"));

			$this->getDoctrine()->getManager()->flush();

			$this->addFlash('success', 'Запись обновленна');

			return $this->redirectToRoute('admin_comment_list', ['id' => $news->getId()	]);
		}

		return $this->render('admin/news/edit_comment.html.twig', [
			'comment' => $comment,
			'form' => $form->createView(),
		]);
	}



	/**
	 * Deletes a Comment entity.
	 *
	 * @Route("/{id}/list/delete", name="admin_comment_delete")
	 * @Method("POST")
	 *
	 * The Security annotation value is an expression (if it evaluates to false,
	 * the authorization mechanism will prevent the user accessing this resource).
	 */
	public function deleteCommentAction(Request $request, Comments $comment)
	{
		if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
			return $this->redirectToRoute('admin_comment_list');
		}

		$em = $this->getDoctrine()->getManager();
		$em->remove($comment);
		$em->flush();

		$this->addFlash('success', 'Запись удалена');
		$news = $comment->getNews();


		return $this->redirectToRoute('admin_comment_list',['id' => $news->getId()]);
	}
}