<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Comments;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 */
class News
{
	const NUM_ITEMS = 10;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var Comments[]|ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *      targetEntity="Comments",
	 *      mappedBy="news",
	 *      orphanRemoval=true
	 * )
	 * @ORM\OrderBy({"dateCreate": "DESC"})
	 */
	private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	private $slug;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="preview_text", type="string", length=255)
     */
    private $previewText;

    /**
     * @var string
     *
     * @ORM\Column(name="detail_text", type="string", length=255)
     */
    private $detailText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_create", type="datetime")
	 * @Assert\DateTime
     */
    private $dataCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_edit", type="datetime")
	 * @Assert\DateTime
     */
    private $dateEdit;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set previewText
     *
     * @param string $previewText
     *
     * @return News
     */
    public function setPreviewText($previewText)
    {
        $this->previewText = $previewText;

        return $this;
    }

    /**
     * Get previewText
     *
     * @return string
     */
    public function getPreviewText()
    {
        return $this->previewText;
    }

    /**
     * Set detailText
     *
     * @param string $detailText
     *
     * @return News
     */
    public function setDetailText($detailText)
    {
        $this->detailText = $detailText;

        return $this;
    }

    /**
     * Get detailText
     *
     * @return string
     */
    public function getDetailText()
    {
        return $this->detailText;
    }

    /**
     * Set dataCreate
     *
     * @param \DateTime $dataCreate
     *
     * @return News
     */
    public function setDataCreate($dataCreate)
    {
        $this->dataCreate = $dataCreate;

        return $this;
    }

    /**
     * Get dataCreate
     *
     * @return \DateTime
     */
    public function getDataCreate()
    {
        return $this->dataCreate;
    }



    /**
     * Set comment
     *
     * @param \AppBundle\Entity\Comments $comment
     *
     * @return News
     */
    public function setComment(Comments $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \AppBundle\Entity\Comments
     */
    public function getComment()
    {
        return $this->comment;
    }

	public function addComment(Comments $comment)
	{
		$comment->setNews($this);
		if (!$this->comment->contains($comment)) {
			$this->comment->add($comment);
		}
	}

	public function removeComment(Comments $comment)
	{
		$comment->setNews(null);
		$this->comment->removeElement($comment);
	}

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comment = new ArrayCollection();
		$this->dataCreate = new \DateTime();
		$this->comment = new ArrayCollection();
    }


    /**
     * Set action
     *
     * @param boolean $action
     *
     * @return News
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return boolean
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set dateEdit
     *
     * @param \DateTime $dateEdit
     *
     * @return News
     */
    public function setDateEdit($dateEdit)
    {
        $this->dateEdit = $dateEdit;

        return $this;
    }

    /**
     * Get dateEdit
     *
     * @return \DateTime
     */
    public function getDateEdit()
    {
        return $this->dateEdit;
    }
}
