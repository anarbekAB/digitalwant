<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Form;

use AppBundle\Entity\News;
use AppBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$builder
			->add('name', null, [
				'attr' => ['autofocus' => true, 'placeholder' => "Название элемента",],
				'label' => 'Название новости',
			])
			->add('previewText', TextareaType::class, [
				'attr' => ['rows' => 8, 'placeholder' => "Текст в списке элементов",],
				'label' => 'Тест для превью',
			])
			->add('detailText', TextareaType::class, [
				'attr' => ['rows' => 8, 'placeholder' => "Текст в детальной странице элемента",],
				'label' => 'Текст для делальной страницы',
			])
			->add('dateEdit', DateTimePickerType::class, [
				'label' => 'Дата публикации',
			])
			->add('action', CheckboxType::class , [
				'label' => 'Статус публикации',
				'required' => false,
			])
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => News::class,
		]);
	}
}
